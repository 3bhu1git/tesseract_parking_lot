const schedule = require('node-schedule');
const max_slot = 120;
const privileged_share = 20;
var expiry_time = 2; // 30 mins validity to expire slot

// to clean up expires booking sessions. runs every minute
module.exports = (app) => {
  var bookingregistry = app.models.booking_registry;
  var waitinglist = app.models.waiting_list;
  console.log("running job scheduler..");
  var job = schedule.scheduleJob('*/1 * * * *', function() {
    let reset_row = {emp_id: null, expire_on: null, booked_time: null }
    let date_now = new Date().toISOString()
    bookingregistry.updateAll({expire_on: {lte: date_now}, occupied: false}, reset_row, function(err, res){
      if (err) {
        console.log(err);
      } else {
        console.log("expired booking", res);
      }
    });
  });

  var job = schedule.scheduleJob('*/1 * * * *', function() {
    bookingregistry.find({where: {emp_id: null}, limit: 1}, function(err, free_slot) {
      if (err) console.log(err); else {
        if(free_slot.length > 0) {
          waitinglist.count(function(err, waitinglist_count) {
            if (err) console.log(err); else {
              if(waitinglist_count > 0) {
                if(waitinglist_count > 0) {
                  waitinglist.find({where: {privileged: true}, limit: 1}, function(err, privileged) {
                    if (err) {
                      console.log(err);
                    }
                    else {
                      let condition = {};
                      if (privileged.length > 0) {
                        condition = {where: {privileged: true}, limit: 1}
                      } else {
                        condition = {where: {privileged: false}, limit: 1}
                      }
                      waitinglist.find(condition, function(err, waitinglist_entry) {
                        if (err) console.log(err); else {
                          if (waitinglist_entry.length > 0) {
                            free_slot = free_slot[0];
                            let promote = {};
                            promote['emp_id'] = waitinglist_entry[0]['emp_id'];
                            promote['privileged'] = waitinglist_entry[0]['privileged'];
                            promote['booked_time'] = new Date().toISOString();
                            promote['expire_on'] = module.exports.getExpiryTime(promote['booked_time']);
                            promote['slot_id'] = free_slot['slot_id'];
                            bookingregistry.updateAll({slot_id: {eq: promote['slot_id']}}, promote, function(err, res) {
                              if (err) console.log(err); else {
                                waitinglist.destroyById(waitinglist_entry[0]['id'], function(err, msg) {
                                  if (err){
                                    return err;
                                  } else {
                                    console.log("moved from waiting list to booking registry");
                                  }
                                });
                              }
                            });
                          } else {console.log("No more entries in waiting list");}
                        }
                      });
                    }
                  });
                }
              }
            }
          });
        }
      }
    });
  });

  //get the expiry time
  module.exports.getExpiryTime = function(date, cb) {
    const dateObj = new Date(date);
    const newDateInNumber = dateObj.setMinutes(dateObj.getMinutes() + expiry_time);
    const processedTime = new Date(newDateInNumber).toISOString();
    return processedTime;
  }
}
