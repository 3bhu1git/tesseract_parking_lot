// create a parking slot of 120
module.exports = function(app) {
  const max_slot = 120; // max parking slot
  const privileged_share = 20; // % reserved for special abled and pregnant employees
  var slot = app.models.booking_registry;
  var res_privileged = new Boolean(false)
  slot.count(function(err, slot_count) {
    if (err) console.log(err); else {
      if(slot_count < max_slot){
        for (i = 1; i <= 120; i++) {
          if(i <= max_slot*privileged_share/100) res_privileged = true; else res_privileged = false;
          let user_data = { slot_id: i, privileged: res_privileged, emp_id: null};
          slot.create(user_data, function(err, res) {
            if (err) console.log(err);
          });
        }
        slot.count(function(err, slot_count) {console.log(slot_count);});
      }
    }
  });
}
