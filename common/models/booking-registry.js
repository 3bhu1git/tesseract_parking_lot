'use strict';
var app = require('../../server/server');
var async = require('async');

var expiry_time = 2; // 30 mins validity to expire slot
const total_slots = 120;
const privileged_share = 20;
var user_req = {};
// After booking a slot create a entry in user_log and calculate the ttl
module.exports = function(Bookingregistry) {
  let slot_id = 0;
  let condition = '';
  Bookingregistry.validatesUniquenessOf('slot_id');
  Bookingregistry.validatesUniquenessOf('emp_id');
  Bookingregistry.book = function(req, cb) {
    user_req = req;
    let find_slot = ''
    Bookingregistry.count({emp_id: req['emp_id']}, function(err, res) { // check if the employee already booked a slot
      if (err) { cb(null, err); } else {
        if (res >= 1) { cb(null, "Already Booked") } else {
          let booking_privilege = req['privileged'];
          Bookingregistry.getDataSource().connector.connect(function (err, db) {
            var bookingregistry = db.collection('booking_registry');
            if (booking_privilege) {
              Bookingregistry.find({where: {emp_id: {neq: null}, privileged: true}}, function(err, total_privilege_slots) { // if the privilege area is filled
                if (err) { cb(null, err);} else {
                  if(total_privilege_slots.length <= total_slots*privileged_share/100) { condition = {where: {and: [{emp_id: null}, {privileged: true}]}, limit: 1}; }
                  else { condition = {where: {and: [{emp_id: null}, {privileged: false}]}, limit: 1}; }
                }
              });
            } else {
              condition = {where: {and: [{emp_id: null}, {privileged: false}]}, limit: 1};
            }
            // find available slot
            Bookingregistry.find(condition, function(err, res){
              if (err) {
                cb(null, err)
              } else {
                if (res.length >= 1) {
                  res = res[0];
                  res['emp_id'] = req['emp_id'];
                  res['booked_time'] = new Date().toISOString();
                  res['expire_on'] = module.exports.getExpiryTime(res['booked_time']);
                  Bookingregistry.upsert(res, function(err, res) {
                    if (err) {
                      cb(null, err);
                    }
                    else {
                      // maintain user log
                      var userlog = app.models.user_log;
                      let user_log_data = {emp_id: userlog['emp_id'], booked_time: res['booked_time'], slot_id: res['slot_id'], privileged: res['privileged']};
                      userlog.create(user_log_data, function(err, res) {
                        if (err) console.log(err);
                      });
                      cb(null, res);
                    }
                  });
                }
                else {
                  // cb(null, "No slot Available")
                  module.exports.pushToWaitingList (user_req, function ( err, response ){
                    cb(null, response);
                  });
                }
              }
            });
          });
        }
      }
    });
  };

  Bookingregistry.remoteMethod(
    'book',
    {
      http: {path: '/book', verb: 'post'},
      accepts: {arg: 'req', type: 'object', required: true},
      returns: {arg: 'res', type: 'object'},
    }
  );

  //check for the slot status
  Bookingregistry.check_slot = function(id, cb) {
    Bookingregistry.find({where: {'id': id}}, function(err, booking_record) {
      if (err)
        return err;
      else {
        if (booking_record.length > 0) {
          if (module.exports.checkExpiry(booking_record[0]['booked_time'])) {
            let msg = "";
            // Bookingregistry.destroyById(id, function(err, msg) {
            //   if (err){
            //     return err;
            //   }
            // });
            cb(null, {"message":"Slot Expired, please book again"});
          }
          else {
            cb(null, booking_record);
          }
        }
        else {
          cb(null, "No record found");
        }
      }
    });
  };

  Bookingregistry.remoteMethod(
    'check_slot',
    {
      http: {path: '/check_slot/:id', verb: 'get'},
      accepts: {arg: 'id', type: 'string', required: true},
      returns: {arg: 'data', type: 'object'},
    }
  );

  // parking done
  Bookingregistry.parked = function(id, cb) {
    Bookingregistry.find({where: {'id': id}}, function(err, booking_record) {
      if (err)
        return err;
      else {
        if (booking_record.length > 0) {
          if(module.exports.checkExpiry(booking_record[0]['booked_time']))
            cb(null, "Slot Expired, please book again!");
          else {
            booking_record = booking_record[0];
            booking_record['occupied'] = true;
            Bookingregistry.upsert(booking_record, function(err, res) {
              if (err) {
                cb(null, err);
              }
              else {
                // parked succesfully
                cb(null, res);
              }
            });
          }
        }
        else {
          cb(null, "You have not booked the slot, please book before 15 mins prior!");
        }
      }
    });
  };

  Bookingregistry.remoteMethod(
    'parked',
    {
      http: {path: '/parked/:id', verb: 'post'},
      accepts: {arg: 'id', type: 'string', required: true},
      returns: {arg: 'data', type: 'object'},
    }
  );

  // check for the expiry
  module.exports.checkExpiry = function(booked_time, slot_used) {
    // Bookingregistry.find({where:{emp_id: null}}, function(err, used_slot) {
    //   if (err) return err; else {slot_used = used_slot.length; return slot_used}
    // });
    // // check if half of the slot is filled then reduce the waiting time to half
    // let expirytime = 0;
    // console.log(slot_used);
    // if (slot_used > total_slots/2)
    //   expirytime = expiry_time/2;
    //   console.log(expirytime);
    // console.log("Expiry time reduced to half of actual, since the occupancy is more than half");
    const dateObj = new Date(booked_time);
    const newDateInNumber = dateObj.setMinutes(dateObj.getMinutes() + expiry_time);
    const ttl = new Date(newDateInNumber).toISOString();
    if (new Date(ttl).getTime() / 1000 < new Date().getTime() / 1000) return true; else return false;
  }

  // check total slot availability
  Bookingregistry.available_slots = function(cb) {
    Bookingregistry.find({where:{emp_id: null}}, function(err, occupied) {
      if (err) cb(null, err); else cb(null, occupied.length);
    });
  };

  Bookingregistry.remoteMethod(
    'available_slots',
    {
      http: {path: '/available_slots', verb: 'get'},
      returns: {arg: 'count', type: 'object'},
    }
  );

  // check total slot occupied
  Bookingregistry.occupied_slots = function(cb) {
    Bookingregistry.find({where:{emp_id:{neq:null}}}, function(err, occupied) {
      if (err) cb(null, err); else cb(null, occupied.length);
    });
  };

  Bookingregistry.remoteMethod(
    'occupied_slots',
    {
      http: {path: '/occupied_slots', verb: 'get'},
      returns: {arg: 'count', type: 'object'},
    }
  );

  module.exports.pushToWaitingList = function(request, cb) {
    var waitinglist = app.models.waiting_list;
    let user_data = {emp_id: request['emp_id'], privileged: request['privileged']};
    waitinglist.create(user_data, function(err, res) {
      if (err) console.log(err); else {
        cb(null, res);
      }
    });
  }

  //get the expiry time
  module.exports.getExpiryTime = function(date, cb) {
    const dateObj = new Date(date);
    const newDateInNumber = dateObj.setMinutes(dateObj.getMinutes() + expiry_time);
    const processedTime = new Date(newDateInNumber).toISOString();
    return processedTime;
  }

};
